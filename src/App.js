import Footer from './Pages/Footer';
import HeaderContent from './Pages/HeaderContent';

function App() {
  return (
    <div>
      <HeaderContent />
    </div>
  );
}

export default App;
